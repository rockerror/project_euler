def main():
    highest = 0
    for x in xrange(100):
        for y in xrange(100):
            p = x**y
            s = sum(map(int, str(p)))
            if s > highest:
                highest = s
    result = highest
    return result == 972, result