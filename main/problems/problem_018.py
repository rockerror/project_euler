from ..materials.core import path
from ..tools.trianglefolding import fold_triangle

def main():
    with open(path("p018_triangle.txt"), "r") as open_file:
        triangle = [map(int, line.strip().rstrip().split(" ")) for line in open_file]
        triangle.reverse()

    result = fold_triangle(triangle)
    return result == 1074, result



