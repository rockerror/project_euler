import cProfile, os

def one(number, profile=True):
    exec("from main.problems import problem_{} as problem".format(str(number).zfill(3)))
    globals()["problem"] = problem
    cmd = """print "problem_{}", problem.main()""".format(str(number).zfill(3))
    if profile:
        cProfile.run(cmd, sort=2)
    else:
        exec(cmd)

def all_problems():
    problems = sorted(set(os.listdir(os.path.join(os.path.dirname(__file__), "main","problems"))))
    for problem in problems:
        if not "__" in problem and not problem.endswith(".pyc"):
            try:
                one(int(problem.split(".")[0][-3:]), profile=False)
            except:
                print "Could not run problem", problem

# cProfile.run("all_problems()", sort=2)
one(57)