from itertools import permutations

def main():
    perms = permutations(list("0123456789"))
    divisors = [2, 3, 5, 7, 11, 13, 17]

    q = []
    for p in perms:
        if p[0] == 0:
            continue

        for i, d in enumerate(divisors, 1):
            chars = [str(p[i + x]) for x in xrange(3)]
            s = int("".join(chars))
            if s % d:
               break
        else:
            q.append(int("".join(p)))

    result = sum(q)
    return result == 16695334890, result
