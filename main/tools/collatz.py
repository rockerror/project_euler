class RecCollatz(object):

    values = {1:1}

    @classmethod
    def count(cls, n):

        if n in cls.values:
            return cls.values[n]

        elif not n % 2:
            cls.values[n] = 1 + cls.count(n / 2)

        else:
            cls.values[n] = 2 + cls.count( (3 * n + 1) / 2 )

        return cls.values[n]