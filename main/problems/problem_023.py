from ..tools.abundants import abunds

def main():
    n = 28123
    abundants = abunds(n)
    result = sum((i for i in xrange(1, n+1) if not any((i - a in abundants for a in abundants))))
    return result == 4179871, result
