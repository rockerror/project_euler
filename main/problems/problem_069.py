import numpy, math

def coprimes():
    yield (2, 1)
    yield (3, 1)
    for m, n in coprimes():
        yield (2*m - n, m)
        yield (2*m + n, m)
        yield (m + 2*n, n)

def main():
    g = coprimes()
    m = 0
    mx = None
    for x in xrange(1500000):
        y = g.next()
        d = float(y[0]) / y[1]
        if d > m:
            m = d
            mx = x
    # print mx
