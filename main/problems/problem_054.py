from ..materials.core import path

import collections

rules = """
1 = High Card: Highest value card.
2 = One Pair: Two cards of the same value.
3 = Two Pairs: Two different pairs.
4 = Three of a Kind: Three cards of the same value.
5 = Straight: All cards are consecutive values.
6 = Flush: All cards of the same suit.
7 = Full House: Three of a kind and a pair.
8 = Four of a Kind: Four cards of the same value.
# Straight Flush: All cards are consecutive values of same suit.
# Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
"""

class Hand(object):
    ordered_kinds = "23456789TJQKA"

    def __init__(self, cards):
        self.inline = "".join(cards)

        self.primary_value = 0
        self.secondary_value = 0
        self.third_value = 0

        suits = self.count_characters(1)
        kinds = self.count_characters(0)
        ordered_kinds = "".join(x for x in self.ordered_kinds if x in kinds)

        self.secondary_value = self.max_kind()

        if len(suits) == 1 and ordered_kinds in self.ordered_kinds[-5:]:
            self.name = "royal flush"
            self.primary_value = 10

        elif len(suits) == 1 and ordered_kinds in self.ordered_kinds:
            self.name = "straight flush"
            self.primary_value = 9

        elif 4 in kinds.values():
            self.name = "Four of a Kind"
            self.primary_value = 8
            self.secondary_value = [x for x in kinds if kinds[x] == 4][0]

        elif 3 in kinds.values() and 2 in kinds.values():
            self.name = "Full House"
            self.primary_value = 7
            self.secondary_value = [x for x in kinds if kinds[x] == 3][0]

        elif len(suits) == 1:
            self.name = "Flush"
            self.primary_value = 6

        elif ordered_kinds in self.ordered_kinds and len(ordered_kinds) == 5:
            self.name = "Straight"
            self.primary_value = 5

        elif 3 in kinds.values():
            self.name = "Three of a kind"
            self.primary_value = 4
            self.secondary_value = [x for x in kinds if kinds[x] == 3][0]

        elif kinds.values().count(2) == 2:
            self.name = "Two pairs"
            self.primary_value = 3
            pairs = [x for x in kinds if kinds[x] == 2]
            indices = [self.ordered_kinds.index(x) for x in pairs]
            self.secondary_value = max(indices)
            self.third_value = min(indices)

        elif 2 in kinds.values():
            self.name = "One pair"
            self.primary_value = 2
            self.secondary_value = [self.ordered_kinds.index(x) for x in kinds if kinds[x] == 2][0]
            self.third_value = max(self.ordered_kinds.index(x) for x in kinds if self.ordered_kinds.index(x) != self.secondary_value)

        else:
            self.name = "Highest Card"
            self.primary_value = 1



    def count_characters(self, start):
        characters = collections.defaultdict(lambda : 0)
        for character in self.inline[start::2]:
            characters[character] += 1
        return dict(characters)

    def max_kind(self):
        return max(self.ordered_kinds.index(x) for x in self.inline[0::2])

def main():
    with open(path("p054_poker.txt"), "r") as open_file:
        both_hands = [line.strip().rstrip().split(" ") for line in open_file]

    hand_a_wins, hand_b_wins = 0, 0
    results = collections.defaultdict(lambda : 0)

    for both_hand in both_hands:
        hand_a, hand_b = Hand(both_hand[:5]), Hand(both_hand[5:])


        results[hand_a.name] += 1
        results[hand_b.name] += 1

        if hand_a.primary_value > hand_b.primary_value:
            hand_a_wins += 1

        elif hand_a.primary_value == hand_b.primary_value:
            if hand_a.secondary_value > hand_b.secondary_value:
                hand_a_wins += 1

            elif hand_a.secondary_value == hand_b.secondary_value:

                if hand_a.third_value > hand_b.third_value:
                    hand_a_wins += 1
                elif hand_a.third_value == hand_b.third_value:
                    pass
                else:
                    hand_b_wins += 1
            else:
                hand_b_wins += 1

        else:
            hand_b_wins += 1

    result = hand_a_wins
    return result == 376, result


