from ..tools import geometricnumbers

def main():
    m = 10000000000
    t = set(geometricnumbers.triangle_number_until(m))
    p = set(geometricnumbers.pentagon_numbers_until(m))
    h = set(geometricnumbers.hexagon_numbers_until(m))

    tpi = t.intersection(p)
    tphi = tpi.intersection(h)

    result = max(tphi)
    return result == 1533776805, result
