"""
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
"""

# from itertools import permutations
from ..tools.primes import seive

def rotations(n):
    yield int(n)
    for x in n:
        n = n[1:] + x
        yield int(n)

def main():
    primes = seive(1000000)
    ps = {2,5}
    bad_chars = "024685"


    for prime in primes:

        string_prime = str(prime)

        skip = False
        for char in string_prime:
            if char in bad_chars:
                skip = True
                break
        if skip:
            continue

        pp = set(rotations(string_prime))

        if pp.issubset(primes):
            ps.update(pp)

    result = len(list(set(ps)))
    return result == 55, result