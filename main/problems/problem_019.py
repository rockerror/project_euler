def daygen():
    while 1:
        for x in xrange(6):
            yield False
        yield True

def main():
    """

    You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
    How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

    """
    standard = [31, #jan
                28, #feb
                31, #mar
                30, #apr
                31, #may
                30, #jun
                31, #jul
                31, #aug
                30, #sep
                31, #oct
                30, #nov
                31, #dec
               ]
    leap = standard[:]
    leap[1] = 29

    c = 0

    day = daygen()
    for year in xrange(1901, 2001):
        l = year % 4
        months = standard
        if not l:
            months = leap
            if year % 100:
                months = standard
                if year % 400:
                    months = leap

        for month in months:
            for d in xrange(month):
                D = day.next()

                if D and d == 1:
                    c += 1
    result = c
    return result == 171, result



