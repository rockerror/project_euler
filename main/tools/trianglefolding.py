def fold_triangle(triangle):
    for y, line in enumerate(triangle):
        try:
            triangle[y + 1]
        except:
            break
        for x, number in enumerate(line):
            try:
                triangle[y][x + 1]
            except:
                break
            triangle[y+1][x] = triangle[y+1][x] + max((triangle[y][x], triangle[y][x+1]))
    return triangle[-1][0]