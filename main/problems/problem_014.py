from ..tools.collatz import RecCollatz

def main():
    n, l = None, None

    for x in xrange(1, 1000001):
        tl = RecCollatz.count(x)
        if tl > l:
            l = tl
            n = x

    result = n

    return result == 837799, result