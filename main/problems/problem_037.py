from ..tools.primes import seive

def main():
    primes = seive(1000000)
    ps = {2,5}
    bad_chars = "0468"

    found = []

    for prime in primes:

        string_prime = str(prime)

        skip = False
        for char in string_prime:
            if char in bad_chars:
                skip = True
                break
        if skip:
            continue

        c1 = string_prime[:]
        c2 = string_prime[:]
        for x in string_prime[:-1]:
            c1, c2 = c1[:-1], c2[1:]
            if not int(c1) in primes:
                break
            if not int(c2) in primes:
                break
        else:
            found.append(prime)

    result = sum(found[4:])

    return result == 748317, result