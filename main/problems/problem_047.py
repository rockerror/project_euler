"""
The first two consecutive numbers to have two distinct prime factors are:
The first three consecutive numbers to have three distinct prime factors are:
Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?
"""

import math
from ..tools.primes import seive

def prime_divisors(primes, n):
    sqrt = math.ceil(n/2.0)
    divs = []
    for x in primes:
        if not n % x:
            divs.append(x)
        if x > sqrt:
            break
    return divs



def main():
    big_number = 1000000
    searching_for = 4

    primes = sorted(seive(big_number))

    slots = list()
    numbers = list()

    for n in xrange(1, big_number):
        divs = prime_divisors(primes, n)
        if len(divs) == searching_for:
            if not divs in slots:
                slots.append(divs)
                numbers.append(n)
            else:
                slots = list()
                numbers = list()
        else:
            slots = list()
            numbers = list()

        if len(slots) == searching_for:
            break

    result = min(numbers)

    return result == 134043, result