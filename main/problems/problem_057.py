from fractions import Fraction
import math
import decimal

context = decimal.Context(prec=10000)
decimal.setcontext(context)

D = decimal.Decimal


def main():
    amount = 0

    for x in xrange(1, 1000):

        dec =  D(1.0) + D(expansion(D(x)))

        frac = Fraction(dec)

        numerator, denominator = frac.numerator, frac.denominator

        print x

        if len(str(numerator)) > len(str(denominator)):
            amount += 1

    return amount

def expansion(n, f=.5):
    if n == 0:
        return D(.5)
    else:
        return D(1.0) / (D(2.0) + expansion(D(n) - D( 1), D(f) * D(.5)))
