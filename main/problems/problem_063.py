from operator import add

def main():
    ndict = dict()
    for x in xrange(1, 50):
        ndict[x] = []
        y = 1
        yp = 1
        while yp < int("9"*x):

            yp = pow(y, x)

            yps = str(yp)

            if len(yps) == x:
                ndict[x].append(yp)
            y += 1
    result = sum((len(x) for x in ndict.values()))

    return result == 49, result
