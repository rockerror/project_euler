def fib(n):
    a, b, c = 1, 0, 0
    while a+b <= n:
        c = a + b
        a, b = b, c
        yield c