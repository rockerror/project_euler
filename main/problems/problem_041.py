from ..tools.primes import check
from itertools import permutations

def main():
    ns = "987654321"
    for z in ns:
        r = sorted(permutations(map(int, list(ns))))
        r.reverse()

        f = False

        e = {0,2,4,6,8}

        for p in r:

            if p[-1] in e:
                continue

            if check(int("".join(map(str, p)))):
                f = p
                break

        if f:
            break

        ns = ns[1:]

    result = int("".join(map(str, f)))

    return result == 7652413, result
