def main():
    units = {
                    "1":"one ",
                    "2":"two ",
                    "3":"three ",
                    "4":"four ",
                    "5":"five ",
                    "6":"six ",
                    "7":"seven ",
                    "8":"eight ",
                    "9":"nine ",}
    teens = {       "10":"ten ",
                    "11":"eleven ",
                    "12":"twelve ",
                    "13":"thirteen ",
                    "14":"fourteen ",
                    "15":"fifteen ",
                    "16":"sixteen ",
                    "17":"seventeen ",
                    "18":"eighteen ",
                    "19":"nineteen ",}
    tens = {
                    "2":"twenty ",
                    "3":"thirty ",
                    "4":"forty ",
                    "5":"fifty ",
                    "6":"sixty ",
                    "7":"seventy ",
                    "8":"eighty ",
                    "9":"ninety ",}
    thousand = "thousand "
    hundred = "hundred "
    t = ""
    for x in xrange(1, 1001):
        teen_used, epic_used, ten_or_unit_used = False, False, False
        s = str(x).zfill(4)
        p = ""
        if s[0] != "0":
            epic_used = True
            p += units[s[0]] + thousand
        if s[1] != "0":
            epic_used = True
            p += units[s[1]] + hundred
        if s[2] != "0":
            ten_or_unit_used = True
            if s[2] != "1":
                p += tens[s[2]]
            else:
                teen_used = True
                p += teens[s[2:]]
        if s[3] != "0" and not teen_used:
            ten_or_unit_used = True
            p += units[s[3]]
        if epic_used and ten_or_unit_used:
            p += "and "
        t += p
    result = len(t.replace(" ", ""))
    return result == 21124, result