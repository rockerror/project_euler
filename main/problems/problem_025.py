from ..tools.fib import fib

def main():
    l1 = int("9" * 1000)
    g = fib(l1)
    c = 0
    while True:
        c += 1
        f = g.next()
        if len(str(f)) == 1000:
            break
    result = c
    return result == 4782, result