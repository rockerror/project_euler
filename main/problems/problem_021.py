from ..tools.factors import divisors

def main():
    t = 0
    for b in xrange(1, 10000):
        b_dvs = divisors(b, include_n=False)
        a = sum(b_dvs)
        a_dvs = divisors(a, include_n=False)
        if sum(a_dvs) == b:
            if not a == b:
                t += b
    result = t
    return result == 31626, result