from ..tools.factors import divisors

def doit():
    triangle = 0
    count = 1
    while True:
        triangle += count
        divs = divisors(triangle)
        if len(divs) > 500:
            return triangle
        count += 1

def main():
    result = doit()
    return result == 76576500, result