import itertools
from ..tools.primes import seive

def doit():
    s = seive(100000)

    count = 0

    for prime in sorted(s)[1:]:

        perms = itertools.permutations(list(str(prime)))

        perm_ints = [int("".join(map(str, x))) for x in perms]

        prime_perms = sorted([x for x in perm_ints if x in s])

        prime_perms_a = prime_perms[prime_perms.index(prime):]

        for second_prime in prime_perms_a:

            prime_perms_b = prime_perms_a[:]

            prime_perms_b.pop(prime_perms_b.index(second_prime))

            for third_prime in prime_perms_b:

                if len({prime, second_prime, third_prime}) != 3:
                    continue

                ab = abs(prime - second_prime)

                bc = abs(second_prime - third_prime)

                if ab == bc:

                    count += 1

                    if count == 2:
                        return int("".join(map(str, [prime, second_prime, third_prime])))

def main():
    result = doit()
    return result == 296962999629, result
