from operator import mul

def main():
    positions = [1, 10, 100, 1000, 10000, 100000, 1000000]
    s = "".join(map(str, xrange(max(positions))))
    result = reduce(mul, [int(s[p]) for p in positions], 1)
    return result == 210, result