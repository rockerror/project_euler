"""
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.

"""

import re, decimal

def rematches(str_dec):
    regex = r"(\d+)\1"
    return re.match(regex, str_dec, re.MULTILINE)

def recursive_matches(str_dec):
    match = rematches(str_dec)
    if hasattr(match, "group"):
        if match.group(1):
            return recursive_matches(match.group(1))
        else:
            return str_dec
    else:
        return str_dec

def remove_leading_zeros(n):
    z = str(n)
    i = int(n)
    s = str(i)
    return s


def is_same(n):
    return n.count(n[0]) == len(n)

def is_alternating(n):
    return n.count(n[:2]) == len(n)/2

def main():
    decimal.getcontext().prec = 2000
    one = decimal.Decimal(1)

    highest = 0
    highestn = 0

    for x in xrange(2, 1001):

        str_dec_short = str(1.0 / x).split(".")[-1][1:-1]

        if len(str_dec_short) < 9:
            continue

        if is_same(str_dec_short):
            continue

        if is_alternating(str_dec_short):
            continue

        str_dec_long = remove_leading_zeros(str(one / x).split(".")[-1][:-1])
        match = recursive_matches(str_dec_long)

        if len(match) != 1999:
            if len(match) > highest:
                highest = len(match)
                highestn = x



    result = highestn

    return result == 983, result










def pattern_finder(pattern):
    p_length = len(pattern)
    for a in xrange(p_length/2):
        for b in xrange(a+1, p_length/2):
            p_slice = pattern[a:b]
            p_s_length = abs(a-b)
            n = p_length / p_s_length
            match = p_slice*n
            m_length = len(match)
            if match == pattern[:m_length]:
                if pattern.count(p_slice) > 1:
                    return p_slice


def old():

    decimal.getcontext().prec = 5000
    one = decimal.Decimal(1)

    highest = 0
    highest_x = 0

    pattern_maker = lambda y:str(int(str(one / y).split(".")[-1]))

    for x in xrange(1,1000):
        basic_length = len(str(1.0/x))
        if basic_length == 16:
            pattern = pattern_maker(x)
            if len(pattern) > 1900:
                plen = len(str(pattern_finder(pattern)))
                if plen > highest:
                    highest = plen
                    highest_x = x
    result = highest_x
    return result == 983, result



