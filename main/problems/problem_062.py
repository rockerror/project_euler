import math, itertools, collections

def main():
    result = find()
    return result == 127035954683, result

def find():
    cubes = {int(math.pow(x, 3)) for x in xrange(2, 9000)}
    incubes = collections.defaultdict(list)
    for cube in cubes:
        key = "".join(sorted(str(cube)))
        incubes[key].append(cube)
        if len(incubes[key]) == 5:
            return min(incubes[key])
