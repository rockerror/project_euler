"""
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
"""

setit = lambda x: set(tuple(str(x)))

def main():

    for x in xrange(1, 300000):
        base = setit(x)
        for y in xrange(2, 7):
            mult = setit(x*y)
            if not mult == base:
                break
        else:
            break

    result = x
    return result == 142857, result
