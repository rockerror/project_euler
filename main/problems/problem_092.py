import math

KNOWN = set()
RESULTS = dict()

class NonRepeatingChain(object):

    def __init__(self, n):
        self.result = None
        self.chain = list()
        self.length = 0
        self.run(n)

    def sum_square_of_digits(self, n):
        digits = map(int, str(n))
        new_n = sum(map(lambda x: x ** 2, digits))
        return new_n

    def update(self):
        KNOWN.update(self.chain)
        self.result = self.chain[-1]
        RESULTS.update({x: self.result for x in self.chain})

    def run(self, n):
        # add our number to the chain
        self.chain.append(n)

        if n == 1 or n == 89:
            # check if we have an ending number
            self.update()
            return

        elif n in KNOWN:
            # or our number is already known to end at a either end
            self.chain.append(RESULTS[n])
            self.update()
            return

        # number must have passed so generate our next number
        new_n = self.sum_square_of_digits(n)

        # run recursive function
        self.run(new_n)

def main():
    result = 0
    for x in xrange(1, 10000000):
        nrc = NonRepeatingChain(x)
        if nrc.result == 89:
            result += 1
    return result == 8581146, result


