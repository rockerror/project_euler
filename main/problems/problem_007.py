from ..tools.primes import seive

def doit():
    oh = 10001
    for x in xrange(oh, oh**2, oh):
        c = seive(x)
        l = len(c)
        if l >= oh:
            return sorted(c)[oh-1]

def main():
    result = doit()
    return result == 104743, result