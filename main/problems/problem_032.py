nums = {"1", "2", "3", "4", "5", "6", "7", "8", "9"}

def main():
    c = set()
    identity = set(map(str, xrange(1,10)))
    for x in xrange(1,9999):
        for y in xrange(x, 9999):
            z = x*y
            s = "{x}{y}{z}".format(x=x,y=y,z=z)
            if len(s) > 9:
                break
            elif set(list(s)) == identity:
                c.add(z)
    result = sum(c)
    return result == 45228, result
