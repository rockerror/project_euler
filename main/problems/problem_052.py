def main():
    to_find = (2, 3, 4, 5, 6)

    for x in xrange(1, 1000000):
        last_result = None
        storage = [x]
        for y in to_find:
            number = x * y
            result = set(list(str(number)))

            if last_result:
                if result == last_result:
                    storage.append(number)
                else:
                    break

            last_result = result

            if len(storage) == len(to_find):
                break

        if len(storage) == len(to_find):
            break

    result = min(storage)

    return result == 142857, result
