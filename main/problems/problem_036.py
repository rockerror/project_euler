def main():
    ns = []
    for x in xrange(1000000):
        s = str(x)
        if s == s[::-1]:
            b = str(bin(x))[2:]
            if b == b[::-1]:
                ns.append(x)
    result =  sum(ns)
    return result == 872187, result