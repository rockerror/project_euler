def gap_gen():
    c = 1
    g = 2
    while 1:
        for x in xrange(4):
            yield c
            c += g
        g += 2


def main():
    t = 1001 * 1001
    g = gap_gen()
    c = 0
    for n in g:
        if n <= t:
            c += n
        else:
            break
    result = c
    return result == 669171001, result
