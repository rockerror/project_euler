import math

def main():
    result = 0
    for n in xrange(1, 101):
        for r in xrange(1, n):
            calc = math.factorial(n) / (math.factorial(r) * math.factorial(n - r))
            if calc > 1000000:
                result += 1
    return result == 4075, result