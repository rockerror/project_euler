import math
from itertools import chain

def divisors(n, include_n=True, include_one=True, return_as_set=True):
    step = 2 if n % 2 else 1
    s = list(chain.from_iterable([i, n / i] for i in xrange(1, int(math.sqrt(n) + 1), step) if not n % i))

    if return_as_set:
        s = set(s)
        if not include_n:
                if n in s:
                    s.remove(n)

        if not include_one:
            if 1 in s:
                s.remove(1)
    else:
        if not include_n:
            if n in s:
                s.pop(s.index(n))
        if not include_one:
            if 1 in s:
                s.pop(s.index(1))
    return s

"""
def sOD(x):
  s = 1
  for i in range(2, int(math.sqrt(x)) + 1):
    if (x % i == 0):
      s += i
      if i != x/i: s += x / i
  return s
"""