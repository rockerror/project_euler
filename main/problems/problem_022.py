from ..materials.core import path

def main():
    names = path("p022_names.txt")
    with open(names, "r") as open_file:
        names = open_file.next().lower().replace('"',"").split(",")
    sorted_names = sorted(names)
    scores = dict(zip("abcdefghijklmnopqrstuvwxyz", xrange(1,27)))
    t = 0
    for i, name in enumerate(sorted_names, 1):
        name_score = sum([scores[l] for l in name]) * i
        t += name_score

    result = t
    return result == 871198282, result