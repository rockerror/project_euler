import os

def path(name):
    return os.path.join( os.path.dirname(__file__), name)