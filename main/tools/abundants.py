from factors import divisors


def abunds(n):
    return {x for x in xrange(1, n+1, 1) if sum(divisors(x, include_n=False)) > x}