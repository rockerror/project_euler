import math

def main():
    result = sum(map(int, str(math.factorial(100))))
    return result == 648, result