# TRIANGLE

def triangle_number(n):
    return int((n*.5)*(n+1))

def triangle_number_until(n, start=1):
    x = start
    t = 1
    yield 1
    while x < n:
        t += 1
        x = triangle_number(t)
        yield x

# PENTAGON

def pentagon_number(n):
    return int(n * ((3 * n -1) * 0.5))

def pentagon_numbers_until(n, start=1):
    x = start
    t = 1
    yield 1
    while x < n:
        t += 1
        x = pentagon_number(t)
        yield x

# HEXAGON

def hexagon_number(n):
    return int(n * (2 * n - 1))

def hexagon_numbers_until(n, start=1):
    x = start
    t = 1
    yield 1
    while x < n:
        t += 1
        x = hexagon_number(t)
        yield x