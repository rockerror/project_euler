from itertools import permutations, izip

def main():
    obj = permutations({0,1,2,3,4,5,6,7,8,9})
    for x, r in izip(xrange(1000000), obj):
        continue
    result = int("".join(map(str, r)))
    return result == 2783915460, result