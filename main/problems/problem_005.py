"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

from operator import mul
import math

def gap(a, b):
    c = a
    while c % b:
        c += a
    return c

def main():
    numbers = range(20, 1, -1)
    while len(numbers) != 1:
        numbers = [gap(a, b) for a,b in zip(numbers[:-1], numbers[1:])]

    result = numbers[0]
    return result == 232792560, result
