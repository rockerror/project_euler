"""
The sum of the squares of the first ten natural numbers is,

The square of the sum of the first ten natural numbers is,

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""

def main():
    sum_of_Squares = lambda y: sum(map(lambda x: x**2, xrange(1, y+1)))
    square_of_sum = lambda y: sum(xrange(1,y+1))**2
    result = abs(sum_of_Squares(100) - square_of_sum(100))
    return result == 25164150, result