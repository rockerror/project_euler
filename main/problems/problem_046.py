from ..tools.primes import seive

def main():
    m = 10000
    s = seive(m)
    ts = [((x ** 2)*2) for x in xrange(m)]
    d = set(xrange(1, m))
    n = set()

    for p in s:
        for t in ts:
            q = t + p
            if q > m:
                break
            n.add(q)

    a = {x for x in  d.difference(n) if x%2}
    a.remove(1)
    result = min(a)
    return result == 5777, result


    return True