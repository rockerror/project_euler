import operator

def main():
    result = int(str(sum(map(lambda x:x**x, xrange(1,1001))))[-10:])
    return result == 9110846700, result
