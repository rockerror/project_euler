from ..materials.core import path

from operator import itemgetter

def main():
    # format the data to find unique numbers, and make lists of attempt digits

    with open(path("p079_passcodeattempts.txt"), "r") as open_file:
        attempts = open_file.next().split(",")

    numbers = set()
    listed_attempts = list()
    for attempt in attempts:
        listed_attempt = map(int, list(str(attempt)))
        listed_attempts.append(listed_attempt)
        numbers.update(listed_attempt)

    # create containers and find outlaying number locations
    first_locations = dict((n, 0) for n in numbers)
    last_locations = dict((n, 0) for n in numbers)
    middle_locations = dict((n, 0) for n in numbers)
    locations = dict((n, 0) for n in numbers)

    for number in numbers:
        for attempt in listed_attempts:
            if attempt[0] == number:
                first_locations[number] -= 1
            if attempt[-1] == number:
                last_locations[number] += 1
            if attempt[1] == number:
                middle_locations[number] += 1

    # get numbers which are never in the middle of attempts
    ends = [k for k, v in middle_locations.iteritems() if v == 0]

    # sum the locations of numbers
    for n in numbers:
        locations[n] += first_locations[n]
        locations[n] += last_locations[n]

    # create the initial passcode
    passcode = [x[0] for x in sorted(locations.items(), key=itemgetter(1))]

    # correct if ending numbers do not appear at ends
    if not passcode[0] in ends:
        n = passcode.pop(1)
        passcode.insert(0, n)

    if not passcode[-1] in ends:
        n = passcode.pop(-2)
        passcode.append(n)

    #	done!
    result =  int("".join(map(str, passcode)))
    return result == 73162890, result

