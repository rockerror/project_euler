def main():
    s = set()
    {s.update({s.add(x**y) for y in xrange(2, 101)}) for x in xrange(2, 101)}
    s.remove(None)
    result = len(list(s))
    return result == 9183, result
