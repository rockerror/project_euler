"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

from ..tools.primes import seive
import math

def doit():
    number = 600851475143
    primes = list(seive(int(math.sqrt(number))))
    primes.reverse()
    for p in primes:
        if number % p == 0:
            return p

def main():
    result = doit()
    return result == 6857, result