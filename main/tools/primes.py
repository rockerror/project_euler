import math

def seive(n):
    p, c, n1 = {2}, set(), n+1
    for x in xrange(3, n1, 2):
        if not x in c:
            c.update(xrange(x+x, n1, x))
            p.add(x)
    return p

def yielding(until):
    prime_count = 2
    primes = {2}
    count = 3
    while prime_count <= until:

        for p in primes:
            if not count % p:
                break
        else:
            yield count
            primes.add(count)
            prime_count += 1
        count += 2

def check(n):

    for x in xrange(2, int(math.sqrt(n) + 1)):
        if not n % x:
            return False
    return True

