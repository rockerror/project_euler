def main():
    c = 0
    for x in xrange(1, 10001):
        l = is_lychrel(x)
        if not l:
            c += 1
    result = c
    return result == 249, result

def is_lychrel(n):
    s, f = n+int(str(n)[::-1]), str(n)
    for x in xrange(49):
        if is_pal(s):
            return s
        f = str(s)[::-1]
        s += int(f)

def is_pal(n):
    s = str(n)
    f = s[::-1]
    return s == f