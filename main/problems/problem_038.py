def main():
    ns = []
    for x in xrange(98765, 1, -1):
        s = str(x)
        for y in xrange(2, 20):
            s += str(y*x)
            if len(s) >= 9:
                break
        if len(s) == 9:
            f = True
            for a in "123456789":
                if not a in s:
                    f = False
                    break
            if f:
                ns.append(int(s))
    result = max(ns)

    return result == 932718654, result