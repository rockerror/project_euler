def doit():
    t = 1000
    t1 = t+1
    for a in xrange(1, t1):
        for b in xrange(a+1, t1):
            s1 = a+b
            if s1 > t:
                break
            s2 = t1 - s1
            for c in xrange(b+1, s2):
                s2 = s1+c
                if s2 > t:
                    break
                if s2 == t:
                    q = ((a ** 2) + (b ** 2)) == c ** 2
                    if q:
                        return a*b*c

def main():
    result = doit()
    return result == 31875000, result



