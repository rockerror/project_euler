def main():
    l = 0
    for x in xrange(10,194980):
        s = sum((y**5 for y in (int(z) for z in str(x))))
        if x == s:
            l += x
    result = l
    return result == 443839, result