from collections import defaultdict
import operator, math



def main():
    pdict = defaultdict(lambda:0)

    p = 1000

    for a in xrange(1, p-1):
        for b in xrange(a, p):
            ab = a+b
            if ab > p:
                break

            h = math.pow(a, 2) + math.pow(b, 2)

            c = math.sqrt(h)

            abc = ab + c

            if abc > p:
                break

            pdict[abc] += 1

    result = max(pdict.iteritems(), key=operator.itemgetter(1))[0]
    return result == 840, result