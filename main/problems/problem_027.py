from ..tools.primes import seive

def main():
    big_seive = seive(100000)

    formular = lambda n, a, b: (n**2) + (a*n) + b
    highest_n = 0
    highest_c = 0

    for a in xrange(-1000, 1000):
        for b in xrange(-1000,1001):
            n = 0
            is_prime = formular(n, a, b)
            while is_prime in big_seive:
                n += 1
                is_prime = formular(n, a, b)
            if n:
                if n > highest_n:
                    highest_n = n
                    highest_c = a * b
    result = highest_c
    return result == -59231, result

