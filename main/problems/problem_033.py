from operator import mul

def lowest_common_terms(a,b):
    did = True
    while did:
        did = False
        for x in xrange(2,a+1):
            if not a % x and not b % x:
                a,b = a/x, b/x
                did = True

    return a,b

def remove_similar_numbers(a,b):
    sa,sb = str(a), str(b)

    for num in "123456789":
        if sa.count(num) == 1 and sb.count(num) == 1:
            na, nb = sa.replace(num, ""), sb.replace(num, "")
            return int(na), int(nb)
    return False, False

def main():
    numers = []
    denoms = []
    for x in xrange(1, 100):
        for y in xrange(x, 100):

            # remove non cancleable
            if x < 11 or y < 11:
                continue

            # remove identical
            if x == y:
                continue

            # trivial cases removed
            if not x % 10 and not y % 10:
                continue

            # can't cancel
            canceled = remove_similar_numbers(x, y)
            if canceled[0] is False:
                continue

            lowest_terms = lowest_common_terms(x, y)
            if (x,y) == lowest_terms:
                continue

            canceled_lowest_terms = lowest_common_terms(*canceled)

            if lowest_terms == canceled_lowest_terms:

                numers.append( lowest_terms[0] )
                denoms.append( lowest_terms[1] )

    result = lowest_common_terms(reduce(mul, numers, 1),reduce(mul, denoms, 1))[-1]

    return result == 100, result
