from ..tools.geometricnumbers import pentagon_numbers_until

def doit():

    this_batch = set(pentagon_numbers_until(10000000, start=1))
    list_batch = sorted(this_batch)
    m = max(this_batch)
    for ix, x in enumerate(list_batch):
        for y in list_batch[ix:]:
            s = x+y

            if s > m:
                continue

            if s in this_batch:
                d = abs(x-y)
                if d > m:
                    continue
                if d in this_batch:
                    return d
#
def main():
    result = doit()
    return result == 5482660, result