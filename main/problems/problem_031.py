def main():

    t = 0
    for two_pounds in xrange(2):
        a = 200 * two_pounds
        if a > 200:
            break

        for one_pounds in xrange(3):
            b = a + (100 * one_pounds)
            if b > 200:
                break

            for fifty_pences in xrange(5):
                c = b + (50 * fifty_pences)
                if c > 200:
                    break

                for twenty_pences in xrange(11):
                    d = c + (20 * twenty_pences)
                    if d > 200:
                        break

                    for ten_pences in xrange(21):
                        e = d + (10 * ten_pences)
                        if e > 200:
                            break

                        for five_pences in xrange(41):
                            f = e + (5 * five_pences)
                            if f > 200:
                                break

                            for two_pences in xrange(101):
                                g = f + (2 * two_pences)


                                if g <= 200:
                                    t += 1
                                else:
                                    break


    result = t
    return result == 73682, result