from ..materials.core import path
import math, itertools, collections

LETTER_KEY = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ"

with open(path("p098_words.txt"), "r") as open_file:
    ORIGINAL_WORDS = set(open_file.next().replace('"', "").split(","))

def word_to_number(word):
    return int(("{}"*len(word)).format(*[LETTER_KEY.index(letter) for letter in word]))

def main():
    anagram_dict = collections.defaultdict(list)
    for word in ORIGINAL_WORDS:
        anagram_dict["".join(sorted(word))].append(word)


    for sorted_letters, words in anagram_dict.copy().iteritems():
        if len(words) < 2:
            del anagram_dict[sorted_letters]

    interesting_dict = collections.defaultdict(list)
    for key, value in sorted(anagram_dict.iteritems()):
        all_numbers = [word_to_number(word) for word in value]
        number_set = {"".join(sorted(str(number))) for number in all_numbers}
        if len(number_set) > 1:
            interesting_dict[key] = value

    for key, value in interesting_dict.iteritems():
        print key, value
