"""
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
"""

from math import factorial

def main():

    strings = map(str,xrange(0, 10))
    factorials = map(factorial, xrange(0, 10))

    f_dict = dict(zip(strings, factorials))

    r = 0

    x = 2
    while x < 100000:
        x += 1
        f = str(x)
        s = sum([f_dict[y] for y in f])
        if s == x:
            r += x

    result = r
    return result == 40730, result