from ..materials.core import path
from ..tools.geometricnumbers import triangle_number_until
import collections



WD = dict(zip(list("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),xrange(1,27)))

def make_word_score(w):
    return sum((WD[l] for l in w))


def main():
    with open(path("p042_codedtrianglenumbers.txt"), "r") as open_file:
        line = open_file.next()
    words = line.replace('"', "").split(",")

    word_score_dict = collections.defaultdict(lambda:0)

    for word in words:
        word_score = make_word_score(word)
        word_score_dict[word_score] += 1

    max_word_score = max(word_score_dict)

    triangle_numbers = list(triangle_number_until(max_word_score))

    result = 0
    for tn in triangle_numbers:
        if tn in word_score_dict:
            result += word_score_dict[tn]

    return result == 162, result
