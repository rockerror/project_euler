import collections, re, operator
from ..tools.primes import seive


def binary_iterator():
    for x in xrange(128):
        yield bin(x)[2:]

def search():
    primes = seive(2000000)
    snums = map(str, xrange(10))

    scores = collections.defaultdict(set)
    for i, prime in sorted(enumerate(primes)):

        s_prime = str(prime)
        for binary in binary_iterator():
            if len(binary) > len(s_prime):
                break

            query = "".join("?" if b == "1" else p for b, p in zip(binary, s_prime))

            for x in snums:
                check = int(query.replace("?", x))
                if check in primes:
                    scores[query].add(check)

                    if len(scores[query]) == 8:
                        return min(scores[query])

def main():
    result = search()
    return result == 121313, result