"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

def palendromic_numbers_from_two_three_digit_numbers():
    numbers = set()
    highest = 0
    for x in xrange(999, 99, -1):
        for y in xrange(x):
            product = x * y
            if product > highest:
                s_product = str(product)
                r_product =s_product[::-1]
                if s_product == r_product:
                    highest = product
                    numbers.add(product)
    return numbers

def main():
    p_numbers = palendromic_numbers_from_two_three_digit_numbers()
    result = sorted(p_numbers)[-1]
    return result == 906609, result